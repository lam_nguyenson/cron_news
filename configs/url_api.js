module.exports = {
    URI_API: 'https://partner.shopeemobile.com',
    AUTHEN_PARTNER: '/api/v1/shop/auth_partner',
    GET_INFO_SHOP: '/api/v1/shop/get',
    GET_ITEMS_LIST: '/api/v1/items/get',
    GET_ITEM_DETAIL: '/api/v1/item/get',
    GET_ORDERS_LIST: '/api/v1/orders/basics',
    GET_ORDER_STATUS: '/api/v1/orders/get',
    GET_ORDER_DETAIL: '/api/v1/orders/detail',
}