module.exports = {
    COMPLETED: 'COMPLETED',
    CANCELLED: 'CANCELLED',
    TO_RETURN: 'TO_RETURN',
    SHIPPED: 'SHIPPED',
    READY_TO_SHIP: 'READY_TO_SHIP'
}