const _ = require('lodash');
const moment = require('moment');
const cron = require('node-cron');
const process = require('process');
const mongoose = require('mongoose');

const ShopController = require('./controller/ShopController');
const ItemController = require('./controller/ItemController');
const OrderController = require('./controller/OrderController');
const OrderDBController = require('./controller/OrderDBController');
const ProductDBController = require('./controller/ProductDBController');
const EcommerceDBController = require('./controller/EcommerceDBController');

const StatusOrder = require('./configs/status_order');

// DB Config
const argv = require('minimist')(process.argv.slice(2));
const config = require(argv.setting);


// Connect to MongoDB
mongoose.connect(config.mongoURI, { useNewUrlParser: true, useFindAndModify: false })
  .then(() => {
    console.log('MongoDB Connected');
  })
  .catch(err => console.log(err));


function processProducts(products, isPlus) {
  _.each(products, (product) => {
    ProductDBController.process(product, isPlus);
  });
}

function readyToShip(shopid, order) {
  try {
    if (order && order.ordersn) {
      OrderDBController.add(order).then((data) => {
        if (data) {
          console.log('completed =>' + order.ordersn);
          OrderController.getDetailShopee(shopid, order.ordersn).then((data_detail) => {
            if (data_detail && data_detail.orders && data_detail.orders[0] && data_detail.orders[0].items) {
              processProducts(data_detail.orders[0].items, false);
            }
          }).catch((err) => {
              console.log(err);
          });
        } else {
          console.log('Order Exist =>' + order.ordersn);
        }
      }).catch((err) => {
        console.log(err);
      });
    }
  } catch(err) {
    console.log(err);
  }
}

function completedAndShipped(order) {
  try {
    if (order && order.ordersn) {
      OrderDBController.delete(order.ordersn).then((data) => {
        if (data) {
          console.log('completed =>' + order.ordersn);
        } else {
          console.log('Not Found =>' + order.ordersn);
        }
      }).catch((err) => {
        console.log(err);
      });
    }
  } catch(err) {
    console.log(err);
  }
}

function cancelledAndToReturn(shopid, order) {
  try {
    if (order && order.ordersn) {
      OrderDBController.delete(order.ordersn).then((data) => {
        if (data) {
          console.log('completed =>' + order.ordersn);
          OrderController.getDetailShopee(shopid, order.ordersn).then((data_detail) => {
            if (data_detail && data_detail.orders && data_detail.orders[0] && data_detail.orders[0].items) {
              processProducts(data_detail.orders[0].items, true);
            }
          }).catch((err) => {
              console.log(err);
          });
        } else {
          console.log('Not Found =>' + order.ordersn);
        }
      }).catch((err) => {
        console.log(err);
      });
    }
  } catch(err) {
    console.log(err);
  }
}


function checkStatusOrder(shopid, orders) {
  _.each(orders, (order) => {
    if (order.order_status === StatusOrder.READY_TO_SHIP) {
      readyToShip(shopid, order);
    } else if ((order.order_status === StatusOrder.COMPLETED) || (order.order_status === StatusOrder.SHIPPED)) {
      completedAndShipped(order);
    } else if ((order.order_status === StatusOrder.CANCELLED) || (order.order_status === StatusOrder.TO_RETURN)) {
      cancelledAndToReturn(shopid, order);
    }
  });
}


cron.schedule('*/30 * * * *', () => {
// cron.schedule('*/2 * * * * *', () => { 
  console.log('running a task every 1 minute => ' + moment().format());
  const from = moment().subtract( 1, 'hours').unix();
  const to = moment().unix();
  try {
    EcommerceDBController.gets({}).then((ecommerces) => {
      console.log(ecommerces);
      _.each(ecommerces, (ecommerce) => {
        const shopid = Number(ecommerce.id_api);
        if (shopid) {
          OrderController.getStatusShopee(shopid, from, to).then((data) => {
            if (data && data.orders && (data.orders.length > 0)) {
              checkStatusOrder(shopid, data.orders);
            }
          }).catch((err) => {
            console.log(err);
          });
        }
      });
    }).catch((err) => {
      console.log(err);
    });
  } catch (err) {
    console.log(err);
  }
});
