# Hau Soyoung Project
### Usage

```
$ https://github.com/Nelife/HauSoyoung.git
```

```sh
$ npm install
```

```sh
# Run for local DB
$ npm run dev

# Run for cloud DB
$ npm run cloud
```
