const _ = require('lodash');
let request = require('request');
const Promise = require('promise');

const Utils = require('../lib/utils');
const URL_API = require('../configs/url_api');

module.exports.getShopInfo = (shopid) => {
    const data_request = {
        shopid: shopid
    };
    const options = Utils.getOptionRequest(URL_API.GET_INFO_SHOP, data_request);
    return Utils.requestPromise(options);
}
