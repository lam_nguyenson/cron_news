const _ = require('lodash');

const Utils = require('../lib/utils');
const URL_API = require('../configs/url_api');

module.exports.getListShopee = (shopid) => {
    const data_request = {
        shopid: shopid
    };
    const options = Utils.getOptionRequest(URL_API.GET_ORDERS_LIST, data_request);
    return Utils.requestPromise(options);
}

module.exports.getStatusShopee = (shopid, create_time_from, create_time_to) => {
    const data_request = {
        shopid: shopid,
        order_status: 'ALL',
        create_time_from: create_time_from,
        create_time_to: create_time_to,
        pagination_entries_per_page: 100,
        pagination_offset: 0
    };
    const options = Utils.getOptionRequest(URL_API.GET_ORDER_STATUS, data_request);
    return Utils.requestPromise(options);
}

module.exports.getDetailShopee = (shopid, order_id) => {
    const data_request = {
        ordersn_list: [
            order_id
        ],
        shopid: shopid
    };
    const options = Utils.getOptionRequest(URL_API.GET_ORDER_DETAIL, data_request);
    return Utils.requestPromise(options);
}