const _ = require('lodash');

// Load Ecommerces model
const Ecommerces = require('../model/ecommerce');


module.exports.gets = (query) => {
    return new Promise(function(resolve, reject) {
        Ecommerces.find({
            status: true
        }).then((ecommerces) => {
            resolve(ecommerces);
        }).catch((err) => {
            reject(err);
        });
    });
}