const _ = require('lodash');

// Load Products model
const Products = require('../model/product');

module.exports.process = (product, isPlus) => {
    return new Promise(function(resolve, reject) {
        if (product && product.variation_sku) {
            Products.findOne({
                'ma_san_pham': product.variation_sku
            }).then((product_db) => {
                if(!product_db) {
                    console.log('Product Not Found!');
                    resolve(false);
                } else {
                    let tempTotal = null; 
                    if (isPlus) {
                        tempTotal = Number(product_db['tong_ton_kho']) + Number(product['variation_quantity_purchased']);
                    } else {
                        tempTotal = Number(product_db['tong_ton_kho']) - Number(product['variation_quantity_purchased']);
                    }
                    Products.update({_id: product_db._id}, {
                        $set: {
                            tong_ton_kho: tempTotal.toString()
                        }
                    }).then((result) => {
                        resolve(true);
                    }).catch((err) => {
                        reject(err);
                    });
                }
            }).catch((err) => {
                console.log(err);
                reject(err);
            });
        }
        
    });
}