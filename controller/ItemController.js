const _ = require('lodash');

const Utils = require('../lib/utils');
const URL_API = require('../configs/url_api');

module.exports.getItemsList = () => {
    const data_request = {
        "pagination_offset": 0,
        "pagination_entries_per_page": 100,
    };
    const options = Utils.getOptionRequest(URL_API.GET_ITEMS_LIST, data_request);
    return Utils.requestPromise(options);
}


module.exports.getItemDetail = (item_id) => {
    const data_request = {
        "item_id": item_id
    };
    const options = Utils.getOptionRequest(URL_API.GET_ITEM_DETAIL, data_request);
    return Utils.requestPromise(options);
}