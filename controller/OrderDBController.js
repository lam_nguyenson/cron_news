const _ = require('lodash');

// Load Orders model
const Orders = require('../model/order');


module.exports.gets = (query) => {
    return new Promise(function(resolve, reject) {
        Orders.find(query).then((orders) => {
            resolve(orders);
        }).catch((err) => {
            reject(err);
        });
    });
}

module.exports.add = (order) => {
    return new Promise(function(resolve, reject) {
        Orders.findOne({
            'ordersn': order.ordersn,
            'deleted': false
        }).then((order) => {
            if(order) {
                resolve(false);
            } else {
                const newOrder = new Orders(order);
                Orders.create(newOrder).then((result) => {
                    resolve(true);
                }).catch((err) => {
                    reject(err);
                });
            }
        }).catch((err) => {
            console.log(err);
            reject(err);
        });
    });
}

module.exports.getById = (ordersn) => {
    return new Promise(function(resolve, reject) {
        Orders.findOne({ordersn: ordersn}).then((order) => {
            resolve(order);
        }).catch((err) => {
            console.log(err);
            reject(err);
        });
    });

}

module.exports.edit = (ordersn, data) => {
    return new Promise(function(resolve, reject) {
        Orders.update({ordersn: ordersn}, {
            $set: data
        }).then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        });
    });
}

module.exports.delete = (ordersn) => {
    return new Promise(function(resolve, reject) {
        Orders.findOne({
            'ordersn': ordersn,
            'deleted': false
        }).then((order) => {
            if(!order) {
                resolve(false);
            } else {
                Orders.update({_id: order._id}, {
                    $set: {
                        deleted: true
                    }
                }).then((result) => {
                    resolve(true);
                }).catch((err) => {
                    reject(err);
                });
            }
        }).catch((err) => {
            console.log(err);
            reject(err);
        });
    });
}