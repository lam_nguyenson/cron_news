const mongoose = require('mongoose');

const ProductTypeSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  id_shopee: {
    type: String,
    required: false
  },
  id_lazada: {
    type: String,
    required: false
  },
  date: {
    type: Date,
    default: Date.now
  }
});

const ProductTypes = mongoose.model('product_types', ProductTypeSchema);

module.exports = ProductTypes;
