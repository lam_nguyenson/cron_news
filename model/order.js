const mongoose = require('mongoose');

const OrderSchema = new mongoose.Schema({
  ordersn: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: false
  },
  status: {
    type: String,
    required: true
  },
  deleted: {
      type: Boolean,
      required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

const Orders = mongoose.model('orders', OrderSchema);

module.exports = Orders;
