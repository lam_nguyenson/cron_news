const mongoose = require('mongoose');

const EcommerceSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  id_api: {
    type: String,
    required: false
  },
  key_api: {
    type: String,
    required: false
  },
  type: {
    type: String,
    required: true
  },
  status: {
    type: Boolean,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

const Ecommerces = mongoose.model('ecommerce', EcommerceSchema);

module.exports = Ecommerces;
