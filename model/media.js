const mongoose = require('mongoose');

const MediaSchema = new mongoose.Schema({
  user_id: {
    type: String,
    required: false
  },
  original_name: {
    type: String,
    required: false
  },
  file_name: {
    type: String,
    required: false
  },
  encoding: {
    type: String,
    required: false
  },
  mimetype: {
    type: String,
    required: false
  },
  path: {
    type: String,
    required: true
  },
  url: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  width: {
    type: Number,
    required: false
  },
  height: {
    type: Number,
    required: false
  },
  date: {
    type: Date,
    default: Date.now
  }
});

const Medias = mongoose.model('medias', MediaSchema);

module.exports = Medias;
