const mongoose = require('mongoose');

const ShopSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: false
  },
  avatar: {
    type: String,
    required: false
  },
  status: {
    type: Boolean,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

const Shops = mongoose.model('shops', ShopSchema);

module.exports = Shops;
