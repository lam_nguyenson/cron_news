const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true
  },
  parent_id: {
    type: String,
    required: false
  }, 
  ma_vach: {
    type: String,
    required: false
  },
  loai_san_pham: {
    type: String,
    required: true
  },
  doanh_nghiep: {
    type: String,
    required: false
  },
  ma_san_pham: {
    type: String,
    required: true
  },
  ten_san_pham: {
    type: String,
    required: true
  },
  ten_khac: {
    type: String,
    required: false
  },
  don_vi_tinh: {
    type: String,
    required: false
  },
  link_anh_san_pham: {
    type: [String],
    required: false
  },
  so_thang_bao_hanh: {
    type: Number,
    required: false
  },
  can_nang_ca_vo_hop: {
    type: String,
    required: false
  },
  gia_nhap: {
    type: String,
    required: false
  },
  gia_ban: {
    type: String,
    required: false
  },
  gia_cu: {
    type: String,
    required: false
  },
  vat: {
    type: String,
    required: false
  },
  loi_nhuan: {
    type: String,
    required: false
  },
  gia_von: {
    type: String,
    required: false
  },
  gia_buon: {
    type: String,
    required: false
  },
  ton_kho: {
    type: String,
    required: false
  },
  tong_ton_kho: {
    type: String,
    required: false
  },
  loi: {
    type: String,
    required: false
  },
  dang_giao_hang: {
    type: String,
    required: false
  },
  ton_trong_kho: {
    type: String,
    required: false
  },
  tam_giu: {
    type: String,
    required: false
  },
  dang_chuyen_kho: {
    type: String,
    required: false
  },
  co_the_ban: {
    type: String,
    required: false
  },
  dat_truoc: {
    type: String,
    required: false
  },
  trang_thai: {
    type: String,
    required: false
  },
  ma_danh_muc: {
    type: String,
    required: false
  },
  danh_muc: {
    type: String,
    required: false
  },
  ma_danh_muc_noi_bo: {
    type: String,
    required: false
  },
  thuong_hieu: {
    type: String,
    required: false
  },
  nha_cung_cap: {
    type: String,
    required: false
  },
  loai_nhap: {
    type: String,
    required: false
  },
  nvkd: {
    type: String,
    required: false
  },
  link_tren_website: {
    type: String,
    required: false
  },
  chieu_dai: {
    type: String,
    required: false
  },
  chieu_rong: {
    type: String,
    required: false
  },
  chieu_cao: {
    type: String,
    required: false
  },
  thu_tu_tren_website: {
    type: String,
    required: false
  },
  date: {
    type: Date,
    default: Date.now
  }
});

const Products = mongoose.model('products', ProductSchema);

module.exports = Products;
