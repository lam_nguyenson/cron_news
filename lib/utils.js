const moment = require('moment');
const Promise = require('promise');
let request = require('request');
const crypto = require("crypto-js");
const URL_API = require('../configs/url_api');
const PARAMETER_SHOP = require('../configs/parameter_shop');


module.exports.getOptionRequest = (url_api, data_request) => {
    const timestamp = moment().unix();
    data_request['partner_id'] = PARAMETER_SHOP.PARTNER_ID;
    // data_request['shopid']     = PARAMETER_SHOP.SHOP_ID;
    data_request['timestamp']  = timestamp;
    const url_api_get = URL_API.URI_API + url_api;
    const signatureBase =  url_api_get + '|' + JSON.stringify(data_request);
    const authorization = crypto.HmacSHA256(signatureBase, PARAMETER_SHOP.PARTNER_KEY).toString();
    const options = {
        url: url_api_get,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': authorization
        },
        body: data_request,
        json: true
    };

    return options;
}

module.exports.requestPromise = (options) => {
    return new Promise(function(resolve, reject) {
        request(options, function(err, res, body) {
            if (err) {
                reject(err);
            }
            resolve(body);
        });
    });
}